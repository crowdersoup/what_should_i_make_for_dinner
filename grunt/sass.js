module.exports = {
    dist: {
        options: {
            style: 'expanded'
        },
        files: {
            'css/app.css': 'css/app.scss'
        }
    }
};
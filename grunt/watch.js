module.exports = {
    files: ['app/app.js', 'app/**/*.js'],
    tasks: [
        'concat:dist',
        'cachebreaker:vendor',
        'cachebreaker:app',
        'cachebreaker:css'
    ],
    options: {
        spawn: false
    }
};
module.exports = {
    vendor: {
        options: {
            match: ['vendors-built.js'], 
            replacement: 'md5',            
            src: {
                path: 'lib/vendors-built.js'
            }
        },
        files: {
            src: ['index.html']
        }
    },
    app: {
        options: {
            match: ['app-built.js'], 
            replacement: 'md5',
            src: {
                path: 'app/app-built.js'
            }
        },
        files: {
            src: ['index.html']
        }
    },
    css: {
        options: {
            match: ['app.css'], 
            replacement: 'md5',
            src: {
                path: 'css/app.css'
            }
        },
        files: {
            src: ['index.html']
        }
    }
};
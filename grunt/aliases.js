// Commands for grunt
module.exports = {
    "default": [
        'bower:install',
        'sass',
        'concat:dist',
        'concat:lib',
        'cachebreaker:vendor',
        'cachebreaker:app'
    ],
    "w": [
        'watch'
    ],
    "release": [
        'bower:install',
        'sass',
        'concat:dist',
        'concat:lib',
        'cachebreaker:vendor',
        'cachebreaker:app'
    ]
};
module.exports = {
    options: {
        separator: '\n',
        sourceMap: true
    },
    lib: {
        src: [
            'lib/jquery/jquery.js',
            'lib/underscore/underscore.js',
            'lib/materialize/js/materialize/materialize.js',
            'lib/toastr/toastr.js',
            'lib/bootbox/bootbox.js',
            'lib/angular/angular.js',
            'lib/moment/moment.js',
            'lib/angular-moment/angular-moment.js',
            'lib/angular-ui-router/angular-ui-router.js',
            'lib/angular-animate/angular-animate.js',
            'lib/ngBootbox/ngBootbox.js',
            'lib/angular-ui-select/select.js',
            'lib/ng-file-upload/angular-file-upload.js',
            'lib/ng-file-upload-shim/angular-file-upload-shim.js',
            'lib/angular-busy/angular-busy.js',
            'lib/stacktrace-js/stacktrace.js'
        ],
        dest: 'lib/vendors-built.js'
    },
    dist: {
        src: ['app/app.js',
              'app/config/*.js',
              'app/controllers/*.js'],
        dest: 'app/app-built.js'
    }
};
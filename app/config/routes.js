(function () {
    'use strict';

    angular
        .module('what_should_i_make_for_dinner')
        .config(configure);

    configure.$inject = ['$urlRouterProvider', '$stateProvider'];
    function configure($urlRouterProvider, $stateProvider) {
        $urlRouterProvider.otherwise('/');
        
        // Setup the routes
        configureRoutes();
        
        function configureRoutes() {
            $stateProvider
                .state('main', {
                    url: '/',
                    templateUrl: 'app/pages/main.html',
                    controller: 'MainController as vm'
                });
        };
    }
})();
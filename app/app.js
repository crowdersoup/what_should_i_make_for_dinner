(function () {
    'use strict';

    var ngModules = [
        'ui.router'
    ];

    angular
        .module('what_should_i_make_for_dinner', ngModules)
        .config(configure)
        .run(initialize);

    configure.$inject = ['$httpProvider', '$logProvider'];

    function configure($httpProvider, $logProvider) {
        $logProvider.debugEnabled(true);

        // NOTE: Routes are located in config/routes.js
    }

    initialize.$inject = ['$rootScope', '$state', '$log'];
    function initialize($rootScope, $state, $log) {
        $log.debug('[app.js] Initializing app (app.js)...');

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {
            $log.debug('[app.js] Changing state to: "' + toState.name + '"');
        });

        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            $log.debug('[app.js] Error while Routing to "' + toState.name + '"...', error);
        });

        $rootScope.$on('$stateNotFound', function (event, toState, toParams, fromState, fromParams) {
            $log.debug('[app.js] Could not find route.');
        });
    }
})();
(function () {
    'use strict';

    angular
        .module('what_should_i_make_for_dinner')
        .controller('MainController', MainController);

    MainController.$inject = ['$scope', '$state'];
    function MainController($scope, $state) {
        var vm = this;
        
        vm.welcomeMessage = "Welcome";
        vm.name = "fair user";

        activate();

        ////////////////

        function activate() {
            vm.name = prompt("Please enter your name", vm.name);
        };
    }
})();